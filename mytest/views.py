from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def test_handler(request):
    return render(request,'index.html')


def name_handler(request):
    context = {
        'username':'name1',
        'password':'12345'
    }
    return render(request,'index1.html',context)


def t1_handler(request):
    context= {
        'key1':'value1',
        'key2':['value1','value2'],
        'key3':{'key3_1':'value3_1','key3_2':'value3_2'},
    }
    return render(request,'t1.html',context)


def t2_handler(request):
    context={
        'hobbys':['java','python','c++']
    }
    return render(request,'t2.html',context)


def t3_handler(request):
    context = {
        'style':2,
        'message':'这是会变色的文章'
    }
    return render(request,'t3.html',context)


def t4_handler(request):
    return render(request,'t4.html')


def re_handler(request,num):
    return HttpResponse(num)


def t5_handler(request):
    context ={
        'html_code': '<a href="//www.edu.csdn.net">CSDN学院</a>',
        'datetime':datetime.now()
    }
    return render(request,'t5.html',context)


def blog_handler(request):
    return render(request,'blog.html')