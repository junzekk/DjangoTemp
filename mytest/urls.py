from django.urls import path, re_path

from mytest import views

urlpatterns = [
    path('', views.test_handler, name='index'),
    path('index',views.name_handler,name='index2'),
    path('t1',views.t1_handler,name='t1'),
    path('t2',views.t2_handler,name='t2'),
    path('t3',views.t3_handler,name='t3'),
    #反向解析
    re_path('re/(\d+)',views.re_handler,name='re'),
    path('t4',views.t4_handler,name='t4'),
    # 过滤器
    path('t5',views.t5_handler,name='t5'),
    path('blog',views.blog_handler,name='blog'),
]

