import datetime

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def index_handler(request):
    return HttpResponse('TempGama.index')


def t1_handler(request):
    result = {
        'key1':'value1',
        'key2':['value1','value2'],
        'key3':{
            'key3_1':'value3_1',
            'key3_2':'value3_2'
        }
    }
    return render(request,'t1.html',result)


def t2_handler(request):
    context ={
        'hobbys': ['java','jpython','dokor']
    }
    return render(request,'t2.html',context)


def t3_handler(request):
    context = {
        'message':'这是一个有思思的句子',
        'style':0
    }
    return render(request,'t3.html',context)


def t4_handler(request):
    return render(request,'t4.html')


def re_handler(request,num):
    return HttpResponse(num)

def t5_handler(request):
    context ={
        'html_code': '<a href="//www.edu.csdn.net">CSDN学院</a>',
        'datetime':datetime.now()
    }
    return render(request,'t5.html',context)


def blog_handler(request):
    return render(request,'blog.html')